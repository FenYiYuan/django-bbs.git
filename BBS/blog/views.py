from django.shortcuts import render, HttpResponse, redirect
from blog.myforms import MyRegForm
from . import models
from django.http import JsonResponse
from PIL import Image, ImageDraw, ImageFont
from io import BytesIO
import random
from django.contrib import auth
from django.contrib.auth.decorators import login_required
import json
from django.db.models import F, Q, Count
from util import pagination
import uuid
from bs4 import BeautifulSoup

# Create your views here.

def get_random():
    return random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)


def get_font():
    li = ['a', 'b', 'd', 'e', 'f', 'g', 'h']
    font = random.choice(li)
    return f'static/font/{font}.ttf'


def get_code(request):
    img_obj = Image.new('RGB', (300, 42), get_random())
    img_draw = ImageDraw.Draw(img_obj)
    img_font = ImageFont.truetype(get_font(), 27)
    code = ''
    for i in range(6):
        random_upper = chr(random.randint(65, 90))
        random_lower = chr(random.randint(97, 122))
        random_int = str(random.randint(0, 9))
        tmp = random.choice([random_lower, random_upper, random_int])
        code += tmp
        img_draw.text((i * 50 + 15, 4), tmp, get_random(), img_font)
    request.session['code'] = code
    io_obj = BytesIO()
    img_obj.save(io_obj, 'png')
    return HttpResponse(io_obj.getvalue())


def paging(cur, all_num, page_num=6):
    pobj = pagination.Pagination(cur, all_num, per_page_num=page_num)
    return pobj


def search(request):
    keyword = request.GET.get('keyword', None)
    if not keyword:
        return render(request, 'blog/index.html', locals())
    article_queryset = models.Article.objects.filter(Q(title__icontains=keyword)
                                                     | Q(desc__icontains=keyword))
    cur_page = request.GET.get('page', 1)
    pobj = paging(cur_page, article_queryset.count())
    page_list = article_queryset[pobj.start:pobj.end]
    return render(request, 'blog/index.html', locals())


def login(request):
    if request.method == 'POST':
        back_dic = {'code': 100, 'url': '', 'error': ''}
        username = request.POST.get('username')
        password = request.POST.get('password')
        code = request.POST.get('code')
        if request.session.get('code').lower() == code.lower():
            user_obj = auth.authenticate(username=username, password=password)
            if user_obj:
                auth.login(request, user_obj)
                back_dic['url'] = '/blog/'
            else:
                obj = models.UserInfo.objects.filter(username=username)
                if obj:
                    back_dic['code'] = 301
                    back_dic['error'] = '用户密码错误'
                else:
                    back_dic['code'] = 200
                    back_dic['error'] = '用户不存在'
        else:
            back_dic['code'] = 300
            back_dic['error'] = '验证码错误'
        return JsonResponse(back_dic)
    return render(request, 'login.html')


def register(request):
    form_obj = MyRegForm()
    if request.method == 'POST':
        back_dic = {"code": 100, 'msg': ''}
        form_obj = MyRegForm(request.POST)
        if form_obj.is_valid():
            clean_data = form_obj.cleaned_data
            clean_data.pop('confirm_password')
            models.UserInfo.objects.create_user(**clean_data)
            back_dic['url'] = '/login/'
        else:
            back_dic['code'] = 200
            back_dic['msg'] = form_obj.errors
        return JsonResponse(back_dic)
    return render(request, 'register.html', locals())


@login_required
def logout(request):
    auth.logout(request)
    return redirect('blog_index')


def index(request):
    article_queryset = models.Article.objects.all()
    cur_page = request.GET.get('page', 1)
    pobj =paging(cur_page, article_queryset.count())
    page_list = article_queryset[pobj.start:pobj.end]
    return render(request, 'blog/index.html', locals())


def site(request, site_name, **kwargs):
    blog = models.Blog.objects.filter(site_name=site_name).first()
    if not blog:
        return render(request, 'error.html')
    article_qs = models.Article.objects.filter(blog=blog).order_by('-create_time')
    if kwargs:
        condition = kwargs.get('condition')
        param = kwargs.get('param')
        # 判断用户到底想按照哪个条件筛选数据
        if condition == 'category':
            article_qs = article_qs.filter(category_id=param)
        elif condition == 'tag':
            article_qs = article_qs.filter(tags__id=param)
        else:
            year, month = param.split('-')
            article_qs = article_qs.filter(create_time__year=year, create_time__month=month)
    cur_page = request.GET.get('page', 1)
    pobj = paging(cur_page, article_qs.count())
    page_list = article_qs[pobj.start:pobj.end]
    return render(request, 'blog/site.html', locals())


def article_datail(request, site_name, article_id):
    blog = models.Blog.objects.filter(site_name=site_name).first()
    article_obj = models.Article.objects.filter(blog=blog, pk=article_id).first()
    if not article_obj:
        return render(request, 'error.html')
    comment_qs = models.Comment.objects.filter(article=article_obj)
    return render(request, 'blog/article.html', locals())


def up_or_down(request):
    if request.is_ajax():
        back_dic = {'code': 100, 'msg': ''}
        if request.user.is_authenticated():
            article_id = request.POST.get('article_id')
            is_up = json.loads(request.POST.get('is_up'))
            article_obj = models.Article.objects.filter(pk=article_id).first()
            if not request.user == article_obj.blog.userinfo:
                is_click = models.UpAndDown.objects.filter(user=request.user, article=article_obj).first()
                if not is_click:
                    if is_up:
                        article_obj.add_up_num()
                        back_dic['msg'] = '支持成功'
                    else:
                        models.Article.objects.filter(pk=article_id).update(down_num=F('down_num') + 1)
                        back_dic['msg'] = '反对成功'
                    models.UpAndDown.objects.create(user=request.user, article=article_obj, is_up=is_up)
                else:
                    back_dic['msg'] = '您已经支持过' if is_click.is_up else '您已经反对过'
                    back_dic['code'] = 200
            else:
                back_dic['code'] = 300
                back_dic['msg'] = '不能推荐自己的内容' if is_up else '不能反对自己的内容'
        else:
            back_dic['code'] = 400
            back_dic['msg'] = '请先<a href="/login/">登陆</a>'
        return JsonResponse(back_dic)
    return render(request, 'error.html')

from django.db import transaction
def comment(request):
    if request.is_ajax():
        back_dic = {'code':100, 'msg':''}
        if request.method == 'POST':
            if request.user.is_authenticated():
                article_id = request.POST.get('article_id')
                content = request.POST.get("content")
                parent_id = request.POST.get('parent_id')
                with transaction.atomic():
                    models.Article.objects.filter(pk=article_id).update(comment_num = F('comment_num') + 1)
                    models.Comment.objects.create(user=request.user,article_id=article_id,content=content,parent_id=parent_id)
                back_dic['msg'] = '评论成功'
            else:
                back_dic['code'] = 200
                back_dic['msg'] = '请先登录在评论'
            return JsonResponse(back_dic)

@login_required
def manage(request, **kwargs):
    blog = request.user.blog
    article_qs = models.Article.objects.filter(blog=blog)
    if kwargs:
        condition = kwargs.get('condition')
        param = kwargs.get('param')
        if condition == 'category':
            article_qs = article_qs.filter(category_id=param)
        elif condition == 'tag':
            article_qs = article_qs.filter(tags__id=param)
        else:
            article_qs = article_qs.filter(category=None)
    cur_page = request.GET.get('page', 1)
    pobj = paging(cur_page, article_qs.count(), page_num=9)
    page_list = article_qs.order_by('-create_time')[pobj.start:pobj.end]
    return render(request, 'backend/manage.html', locals())

import time
def uploadimg(request):
    img_obj = request.FILES.get('editormd-image-file')
    img = Image.open(img_obj)
    uid = ''.join(str(uuid.uuid4()).split('-'))[::4]
    tmp = img_obj.name.rsplit('.', 1)[-1]
    t = str(time.time()).split('.')
    img_name = ''.join([t[0], t[-1], '_', uid, '.', tmp])
    img.save('media/uploadFile/' + img_name)
    # return JsonResponse({"error": 0, "url": f"/media/uploadFile/{img_obj.name}"})
    return JsonResponse({'success':1, 'msg':'上传成功', 'url':f"/media/uploadFile/{img_name}"})


def add_blog(request):
    blog = request.user.blog
    if request.method == 'POST':
        html = request.POST.get('html_doc')
        mkdown = request.POST.get('mark_doc')
        title = request.POST.get('title')
        category_id = request.POST.get('category')
        tags_id = request.POST.getlist('tags[]')
        soup = BeautifulSoup(html, 'html.parser')
        taglist = soup.find_all()
        for tag in taglist:
            if tag.name == 'script':
                tag.decompose()
        desc = soup.text[0:150]
        article_obj = models.Article.objects.create(
                                                title=title,
                                                desc=desc,
                                                content=str(soup),
                                                markdown=mkdown,
                                                blog=request.user.blog,
                                                category_id=category_id)
        article_obj_list = []
        for i in tags_id:
            tag_article_obj = models.Article2Tag(article=article_obj, tag_id=i)
            article_obj_list.append(tag_article_obj)
        models.Article2Tag.objects.bulk_create(article_obj_list)
        return JsonResponse({'code': 100, 'msg':''})

    category_list  = models.Category.objects.filter(blog = blog)
    tag_list = models.Tag.objects.filter(blog=blog)
    return render(request, 'backend/add_article.html', locals())

def del_blog(request, article_id):
    models.Article.objects.filter(pk=article_id).delete()
    return JsonResponse({'code': 100, 'msg': '删除成功'})

def edit_blog(request, article_id):
    blog = request.user.blog
    article_obj = models.Article.objects.filter(pk=article_id).first()
    if request.method == 'POST':
        html = request.POST.get('html_doc')
        mkdown = request.POST.get('mark_doc')
        title = request.POST.get('title')
        category_id = request.POST.get('category')
        tags_id = request.POST.getlist('tags[]')
        soup = BeautifulSoup(html, 'html.parser')
        taglist = soup.find_all()
        for tag in taglist:
            if tag.name == 'script':
                tag.decompose()
        desc = soup.text[0:150]
        models.Article.objects.filter(blog=request.user.blog, pk=article_id).update(
                                                                                title=title,
                                                                                desc=desc,
                                                                                content=str(soup),
                                                                                markdown=mkdown,
                                                                                category_id=category_id)
        models.Article2Tag.objects.filter(article__id=article_id).delete()
        article_obj_list = []
        for i in tags_id:
            tag_article_obj = models.Article2Tag(article_id=article_id, tag_id=i)
            article_obj_list.append(tag_article_obj)
        models.Article2Tag.objects.bulk_create(article_obj_list)
        return JsonResponse({'code': 100, 'msg': ''})
    category_list = models.Category.objects.filter(blog=blog)
    tag_list = models.Tag.objects.filter(blog=blog)
    return render(request, 'backend/edit_article.html', locals())

def category(request):
    category_list = models.Category.objects.filter(blog=request.user.blog)
    if request.method == 'POST':
        name = request.POST.get('c_name')
        if name != '':
            obj = models.Category.objects.filter(name=name, blog=request.user.blog).first()
            if not obj:
                models.Category.objects.create(name=name, blog=request.user.blog)
            return redirect('blog_category')
    return render(request, 'backend/category.html', locals())

def del_category(request, c_id):
    models.Article.objects.filter(blog=request.user.blog, category_id=c_id).update(category=None)
    models.Category.objects.filter(pk=c_id).delete()
    return JsonResponse({'code':100, 'msg': '删除成功'})

def tag(request):
    tag_list = models.Tag.objects.filter(blog=request.user.blog)
    if request.method == 'POST':
        name = request.POST.get('t_name')
        print(name)
        if name != '':
            obj = models.Tag.objects.filter(name=name, blog=request.user.blog).first()
            if not obj:
                models.Tag.objects.create(name=name, blog=request.user.blog)
            return redirect('blog_tag')
    return render(request, 'backend/tag.html', locals())

def del_tag(request, t_id):
    models.Article2Tag.objects.filter(tag_id=t_id).delete()
    models.Tag.objects.filter(blog=request.user.blog, pk=t_id).delete()
    return JsonResponse({'code': 100, 'msg': '删除成功'})