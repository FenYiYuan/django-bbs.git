from django.contrib.syndication.views import Feed
from .models import Article


class LatestEntriesFeed(Feed):
    title = "我的博客网站"
    link = "/siteblogs/"
    description = "最新更新的博客文章！"

    def items(self):
        return Article.objects.order_by('-create_time')[:5]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.desc

