from django import template
from blog import models
from django.db.models import Count
from django.db.models.functions import TruncMonth


register = template.Library()

@register.inclusion_tag('blog/left_menu.html')
def left_menu(site_name):
    blog = models.Blog.objects.filter(site_name=site_name).first()
    newblog = models.Article.objects.filter(blog=blog).order_by('-create_time')[:5]
    category_list = models.Category.objects.filter(blog=blog)\
        .annotate(count_num=Count('article__pk')).values_list('name', 'count_num', 'pk')

    tag_list = models.Tag.objects.filter(blog=blog)\
        .annotate(count_num=Count('article__pk')).values_list('name', 'count_num', 'pk')

    date_list = models.Article.objects.filter(blog=blog)\
        .annotate(month=TruncMonth('create_time')).values('month')\
        .annotate(count_num=Count('pk')).values_list('month', 'count_num')

    return locals()

@register.inclusion_tag('backend/left_menu.html')
def menu(blog):
    tag_qs = models.Tag.objects.filter(blog=blog)
    category_list = models.Category.objects.filter(blog=blog) \
        .annotate(count_num=Count('article__pk')).values_list('name', 'count_num', 'pk')
    return locals()