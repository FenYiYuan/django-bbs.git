from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.

class UserInfo(AbstractUser):
    phone = models.CharField(max_length=11, null=True, verbose_name='电话号码',  blank=True)
    avatar = models.ImageField(upload_to='avatar', default='avatar/default-avatar.png', null=True, blank=True, verbose_name='用户头像')
    create_time = models.DateField(auto_now_add=True, verbose_name='创建时间')
    blog = models.OneToOneField(to='Blog', null=True, verbose_name='用户站点')

    class Meta:
        verbose_name_plural = 'UserInfo' # 修改admin后台表名

    def __str__(self):
        return self.username

class Blog(models.Model):
    site_name = models.CharField(max_length=36, verbose_name='站点名称')
    site_title = models.CharField(null=True, max_length=128, verbose_name='站点标题')
    site_theme = models.TextField(null=True, verbose_name='站点样式')

    class Meta:
        verbose_name_plural = 'Blog' # 修改admin后台表名

    def __str__(self):
        return self.site_name

class Category(models.Model):
    name = models.CharField(max_length=36, verbose_name='文章分类')
    blog = models.ForeignKey(to='Blog', null=True)

    class Meta:
        verbose_name_plural = 'Category' # 修改admin后台表名

    def __str__(self):
        return self.name

class Tag(models.Model):
    name = models.CharField(max_length=36, verbose_name='文章标签')
    blog = models.ForeignKey(to='Blog', null=True)

    class Meta:
        verbose_name_plural = 'Tag' # 修改admin后台表名

    def __str__(self):
        return self.name

class Article(models.Model):
    title = models.CharField(max_length=128, verbose_name='文章标题')
    desc = models.CharField(max_length=255, blank=True, null=True, verbose_name='文章摘要')
    content = models.TextField(verbose_name='文章内容')
    markdown = models.TextField(verbose_name='markdown源码')
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    up_num = models.PositiveIntegerField(default=0, verbose_name='点赞数')
    down_num = models.PositiveIntegerField(default=0, verbose_name='点踩数')
    comment_num = models.PositiveIntegerField(default=0, verbose_name='评论数')

    blog = models.ForeignKey(to='Blog', null=True, verbose_name='用户站点')
    category = models.ForeignKey(to='Category', null=True, verbose_name='文章分类')
    tags = models.ManyToManyField(to='Tag', null=True, through='Article2Tag', through_fields=('article', 'tag'))

    class Meta:
        verbose_name_plural = 'Article' # 修改admin后台表名

    def __str__(self):
        return self.title

    def add_up_num(self):
        self.up_num += 1
        self.save(update_fields=['up_num'])

class Article2Tag(models.Model):
    article = models.ForeignKey(to='Article')
    tag = models.ForeignKey(to='Tag')

    class Meta:
        verbose_name_plural = 'Article2Tag' # 修改admin后台表名

class UpAndDown(models.Model):
    user = models.ForeignKey(to='UserInfo')
    article = models.ForeignKey(to='Article')
    is_up = models.BooleanField()

class Comment(models.Model):
    user = models.ForeignKey(to='UserInfo')
    article = models.ForeignKey(to='Article')
    content = models.CharField(max_length=255, verbose_name='评论内容')
    comment_time = models.DateTimeField(auto_now_add=True)
    parent = models.ForeignKey(to='self', null=True)

    class Meta:
        verbose_name_plural = 'Comment' # 修改admin后台表名


