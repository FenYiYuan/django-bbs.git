from django import forms
from blog import models
from django.core.validators import RegexValidator
from django.contrib import auth

class MyRegForm(forms.Form):
    username = forms.CharField(min_length=2, max_length=9, label='用户名',
                               error_messages={
                                   'min_length': '用户名最少3位',
                                   'max_length': '用户名最大9位',
                                   'required': "用户名不能为空"
                               },
                               widget=forms.widgets.TextInput(attrs={'class': 'username',
                                                                     'placeholder': '用户名',
                                                                     'autocomplete':"off"}))

    password = forms.CharField(min_length=5, max_length=24, label='密码',
                               error_messages={
                                   'min_length': '密码长度不能小于5位',
                                   'max_length': '密码长度最大24位',
                                   'required': "密码不能为空"
                               },
                               widget=forms.widgets.PasswordInput(attrs={'class': 'password',
                                                                         'placeholder': '输入密码',
                                                                         'oncontextmenu':"return false",
                                                                         'onpaste':"return false"},
                                                                  render_value=True))
    confirm_password = forms.CharField(min_length=5, max_length=24, label='确认密码',
                                 error_messages={
                                     'min_length': '密码长度不能小于5位',
                                     'max_length': '密码长度最大24位',
                                     'required': "请确认密码"
                                 },
                                 widget=forms.widgets.PasswordInput(attrs={'class': 'confirm_password',
                                                                           'placeholder': '确认密码',
                                                                           'oncontextmenu':"return false",
                                                                           'onpaste':"return false"},
                                                                    render_value=True))
    email = forms.EmailField(label='邮箱',
                             validators=[
                                 RegexValidator(r'^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$', '邮箱格式错误')],
                             error_messages={
                                 'invalid': '邮箱格式不正确',
                                 'required': "邮箱不能为空"
                             },
                             widget=forms.widgets.EmailInput(attrs={'class': 'email',
                                                                    'placeholder': '输入邮箱',
                                                                    'oncontextmenu':"return false",
                                                                    'onpaste':"return false"}))

    def clean_username(self):
        username = self.cleaned_data.get('username')
        is_exit = models.UserInfo.objects.filter(username=username)
        if is_exit:
            self.add_error('username', '用户名已存在')
        return username

    def clean_email(self):
        email = self.cleaned_data.get('email')
        obj = models.UserInfo.objects.filter(email=email).first()
        if obj:
            self.add_error('email', '邮箱已存在')
        return email

    def clean(self):
        password = self.cleaned_data.get('password')
        confirm_password = self.cleaned_data.get('confirm_password')
        if not password == confirm_password:
            self.add_error('confirm_password', '两次密码不一致')
        return self.cleaned_data


class MyLogin(forms.Form):
    username = forms.CharField(label='用户名',
                               widget=forms.widgets.TextInput(attrs={'class':'username',
                                                                     'placeholder':'用户名',
                                                                     'autocomplete':'off'}))

    password = forms.CharField(label='密码',
                               widget=forms.widgets.PasswordInput(attrs={'class': 'password',
                                                                         'placeholder': '输入密码',
                                                                         'oncontextmenu':"return false",
                                                                         'onpaste':"return false"},
                                                                  render_value=True))

    def clean_username(self):
        username = self.cleaned_data.get('username')
        is_exit = models.UserInfo.objects.filter(username=username)
        if not is_exit:
            return self.add_error('username', '用户不存在')
        return username