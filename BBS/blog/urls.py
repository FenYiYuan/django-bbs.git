from django.conf.urls import url
from blog.views import *

urlpatterns = [
    url(r'^$', index, name='blog_index'),
    url(r'^search/', search, name='blog_search'),
    url(r'^up_or_down/$', up_or_down, name='blog_upordown'),
    url(r'^comment/$', comment, name='blog_comment'),
    url(r'^posts_blog/$', add_blog, name='blog_add'),
    url(r'^delete_blog/(\d+)/', del_blog, name='blog_del'),
    url(r'^edit_blog/(\d+)/', edit_blog, name='blog_edit'),
    url(r'^category/$', category, name='blog_category'),
    url(r'^del_category/(\d+)/', del_category, name='category_del'),
    url(r'^tag/$', tag, name='blog_tag'),
    url(r'^del_tag/(\d+)/', del_tag, name='tag_del'),
    url(r'^(?P<site_name>\w+)/$', site, name='blog_site'),
    url(r'^(?P<site_name>\w+)/article/(?P<article_id>\d+)/', article_datail, name='blog_article'),
    url(r'^(?P<site_name>\w+)/(?P<condition>category|tag|archive)/(?P<param>.*)', site),
]
