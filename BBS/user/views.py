from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from blog import models
from PIL import Image
# Create your views here.

@login_required
def index(request):
    return render(request, 'user/info.html', locals())

@login_required
def setting(request):
    return render(request, 'user/setting.html', locals())


def set_sitename(request):
    if request.is_ajax():
        back_dic = {'code':100, 'msg':''}
        sitename = request.POST.get('sitename')
        sitetitle = request.POST.get('sitetitle')
        obj = models.UserInfo.objects.filter(username=request.user.username).first()
        blog = obj.blog
        if blog:
            blog.site_name = sitename
            blog.save()
            back_dic['msg'] = '站点修改成功'
        else:
            is_exit = models.Blog.objects.filter(site_name=sitename).first()
            if not is_exit:
                blog = models.Blog.objects.create(site_name=sitename, site_title=sitetitle)
                obj.blog = blog
                obj.save()
                back_dic['msg'] = '站点创建成功'
            else:
                back_dic['code'] = 200
                back_dic['msg'] = '站点名已存在'
        return JsonResponse(back_dic)

def set_password(request):
    if request.is_ajax():
        back_dic = {'code': 100, 'msg': ''}
        if request.method == 'POST':
            oldpwd = request.POST.get('oldpwd')
            newpwd = request.POST.get('newpwd')
            rpwd = request.POST.get('rpwd')
            if request.user.check_password(oldpwd):
                if len(newpwd) > 4:
                    if newpwd == rpwd:
                        request.user.set_password(newpwd)
                        request.user.save()
                        back_dic['msg'] = '密码修改成功'
                    else:
                        back_dic['code'] = 200
                        back_dic['msg'] = '两次密码不一致'
                else:
                    back_dic['code'] = 300
                    back_dic['msg'] = '新密码长度小于5个字符'
            else:
                back_dic['code'] = 400
                back_dic['msg'] = '原密码错误'
            return JsonResponse(back_dic)

def set_avatar(request):
    if request.is_ajax():
        back_dic = {'code':100, 'msg':''}
        img_obj = request.FILES.get('avatar')
        if img_obj:
            url = 'avatar/' + img_obj.name
            img = Image.open(img_obj)
            img.save('media/avatar/' + img_obj.name)
            models.UserInfo.objects.filter(username=request.user.username).update(avatar=url)
            back_dic['msg'] = '头像修改成功'
        else:
            back_dic['code'] = 200
            back_dic['msg'] = '请上传头像'
        return JsonResponse(back_dic)