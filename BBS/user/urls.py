from django.conf.urls import url
from user.views import *

urlpatterns = [
    url(r'^$', index, name='user_index'),
    url(r'^setting/$', setting, name='user_setting'),
    url(r'^setting/sitename/', set_sitename, name='set_sitename'),
    url(r'^setting/avatar/', set_avatar, name='set_avatar'),
    url(r'^setting/password/', set_password, name='set_pwd')
]
